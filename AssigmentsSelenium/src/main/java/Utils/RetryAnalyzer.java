package Utils;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryAnalyzer implements IRetryAnalyzer {

    private int minRetryInt = 0;
    private int maxRetryInt = 2;

    public boolean retry(ITestResult result) {
        if (minRetryInt <= maxRetryInt) {
            System.out.println("We are going to retry...");
            minRetryInt = minRetryInt + 1;
            return true;
        } else
            return false;
    }
}
