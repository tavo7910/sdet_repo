package Tests;

import Pages.LoginPage;
import Pages.MenuPage;
import Pages.PimPage;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Test5 {

    WebDriver driver;
    LoginPage objLogin;
    MenuPage objMenu;
    PimPage objPim;

    @BeforeTest
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://opensource-demo.orangehrmlive.com/index.php/auth/login");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Test()
    public void loginFunctionality() throws InterruptedException {

        objLogin = new LoginPage(driver);
        objMenu = new MenuPage(driver);
        objPim = new PimPage(driver);
        SoftAssert softAssert = new SoftAssert();
        Date date = new Date();
        try {
            objLogin.getUserName("Admin");
            objLogin.getPassword("admin123");
            objLogin.clickOnLoginBtn();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            objMenu.getPimTab();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            objPim.getEmployeeName().sendKeys("Linda Anderson");
            objPim.getSearchBtn();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            softAssert.assertEquals(objPim.getFirstNameTab(), "Linda Jane");
            softAssert.assertEquals(objPim.getsNameTb(), "Anderson");
            //File to save as screenshot
            String FileName = date.toString().replace(":", "_").replace(" ", "_") + ".png";
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshot, new File("screenshots\\\\screenshot" + FileName));

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    @AfterTest
    public void close_browser() {
        driver.quit();
    }
}

/*
 Execute following scenario:
  Login to OrangeHRM
  Click on PIM
  Enter
  Employee Name-Linda Anderson in Search box
  Validate and capture screenshot
 */