package Tests;

import Listener.GusListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(GusListener.class)

public class Test3 {

    @Test
    public void testListenerOne() {
        System.out.println("We are going to Run TC1");
    }

    @Test
    public void testListenerTwo() {
        System.out.println("We are going to Run TC2");
    }

    @Test
    public void testListenerThree() {
        System.out.println("We are going to Run TC3");
    }
}
