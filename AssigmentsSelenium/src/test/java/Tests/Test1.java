package Tests;

import Pages.AdminPage;
import Pages.DashboardPage;
import Pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Test1 {
    WebDriver driver;
    LoginPage objLogin;
    DashboardPage objDbPage;
    AdminPage objAdPage;

    @Test()
    public void loginFunctionality() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://opensource-demo.orangehrmlive.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        System.out.println("Page title: " + driver.getTitle());

        objLogin = new LoginPage(driver);
        objDbPage = new DashboardPage(driver);
        objAdPage = new AdminPage(driver);

		System.out.println("---Step 1. Login---");

        objLogin.getUserName("Admin");
        objLogin.getPassword("admin123");
        objLogin.clickOnLoginBtn();
        String headText = objDbPage.getTxtHead();
        Assert.assertEquals(headText, "Dashboard");

        objAdPage.getAdmin().click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		System.out.println("---Step 2. Get text and save it---");

		String txtMnUserMan = objAdPage.getMnUserMan().getText();
        String txtMnJob = objAdPage.getMnJob().getText();
        String txtMnOrg = objAdPage.getMnOrg().getText();
        String txtMnQa = objAdPage.getMnQa().getText();

		System.out.println("---Step 3. Validation---");

		Assert.assertEquals(txtMnUserMan, "User Management", "Text doesn't match");
        Assert.assertEquals(txtMnJob, "Job", "Text doesn't match");
        Assert.assertEquals(txtMnOrg, "Organization", "Text doesn't match");
        Assert.assertEquals(txtMnQa, "Qualifications", "Text doesn't match");

        driver.close();
    }
}

/*
 Login to application again in @Test method, set priority 1 of the same method After logged in
 */

/*
 Click on Admin Link in Home Page and validate following text-
User Management
Job
Organization
Qualifications
*/
