package Tests;

import Pages.AdminPage;
import Pages.LoginPage;
import Utils.RetryAnalyzer;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Test4 {
    //Initializer
    WebDriver driver;
    LoginPage objLogin;
    AdminPage objAdPage;

    @Test(retryAnalyzer = RetryAnalyzer.class, description = "Retry fpr 3 times")
    public void loginFunctionality() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://opensource-demo.orangehrmlive.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        System.out.println("---Step 1. Login---");
        System.out.println("Page title: " + driver.getTitle());
        objLogin = new LoginPage(driver);
        objAdPage = new AdminPage(driver);
        objLogin.getUserName("Admin");
        objLogin.getPassword("admin123");
        objAdPage.getAdmin().click();

        System.out.println("---Step 2. Validating an error to FORCE retry----");
        Assert.assertEquals(true, false);
        driver.quit();
    }
}

/*
Implement Retry analyzer - retry 3 times only for this scenario.
 */
