package Tests;

import Pages.DashboardPage;
import Pages.LoginPage;
import Pages.MenuPage;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Test2 {

    static WebDriver driver;
    LoginPage logPag;
    MenuPage MenuPage;
    DashboardPage dbPage;

    @BeforeTest
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://opensource-demo.orangehrmlive.com/index.php/auth/login");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Test()
    public void LoginTestCase() {

        logPag = new LoginPage(driver);
        MenuPage = new MenuPage(driver);
        dbPage = new DashboardPage(driver);

        System.out.println("---Step 1. Login---");

        logPag.getUserName("Admin");
        logPag.getPassword("admin123");
        logPag.clickOnLoginBtn();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        System.out.println("---Step 2. Get titles---");

        MenuPage.getAdminTab().click();
        System.out.println("Page title Admin :" + driver.getTitle());
        MenuPage.getPimTab();
        System.out.println("Page title PIM:" + driver.getTitle());
        MenuPage.getLeaveTab().click();
        System.out.println("Page title Leave:" + driver.getTitle());
        MenuPage.getDashboardTab().click();
        System.out.println("Page title Leave:" + driver.getTitle());
        MenuPage.getDirecTab().click();
        System.out.println("Page title Directory:" + driver.getTitle());
        MenuPage.getMaintenancePurgeEmployee().click();
        System.out.println("Page title Maintenance:" + driver.getTitle());
        setSize(800, 600);

        MenuPage.getDashboardTab().click();
        System.out.println("Dashboard heading is:" + dbPage.getTxtHead());
        String headText = dbPage.getTxtHead();
        Assert.assertEquals(headText, "Dashboard");

    }

    public static void setSize(int height, int width) {
        System.out.println("---Write a method (avoid using Test annotation) to minimize the window.---");
        Dimension dimension = new Dimension(height, width);
        driver.manage().window().setSize(dimension);
    }

    @AfterTest
    public void closeBrowser() {
        driver.quit();
    }
}

/*
 Read Dashboard heading using [driver.findelement(by.xpath()).gettext()].
 */