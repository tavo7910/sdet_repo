package Labs;

import java.util.Enumeration;
import java.util.Hashtable;

public class HashTable {

	@SuppressWarnings("rawtypes")
	public static void main(String[] args) {
		/*
		Get Set view of keys from HashTable.
		 */

		Hashtable<Integer, String> stringHashtable = new Hashtable<Integer, String>();
		stringHashtable.put(1, "Swetha");
		stringHashtable.put(2, "Jeena");
		stringHashtable.put(3, "Karthika");
		stringHashtable.put(4, "Tony");
		System.out.println("Elements of the Hashtable are: " + stringHashtable);
		Enumeration e = stringHashtable.keys();
		while (e.hasMoreElements()) {
			System.out.println(e.nextElement());
		}
	}

}
