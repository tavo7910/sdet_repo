package Labs;

import java.util.TreeSet;

public class TreeSolution {

    public static void main(String[] args) {
		/*
		 Get highest and lowest value stored in TreeSet
		 */

        TreeSet<Integer> integerTreeSet = new TreeSet<Integer>();
        integerTreeSet.add(7);
        integerTreeSet.add(9);
        integerTreeSet.add(10);
        integerTreeSet.add(5);
        integerTreeSet.add(100);
        integerTreeSet.add(4);
        integerTreeSet.add(680);
        integerTreeSet.add(120);

        System.out.println("The elements of the tree are: " + integerTreeSet);
        System.out.println("Lowest value in the Tree is: " + integerTreeSet.first());
        System.out.println("Highest value in the Tree is: " + integerTreeSet.last());

    }
}
