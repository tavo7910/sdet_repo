package Labs;

import java.util.Scanner;

public class Palindrome {

    public static void main(String[] args) {
		/*
		Write a program to check if a given string is a palindrome or not
		 */
        Scanner scan = new Scanner(System.in);
        System.out.print("Write your String: --> ");
        String stringInput = scan.next();
        String orgStr = stringInput;
        String reverseStringInput = "";
        int len = stringInput.length();

        for (int i = len - 1; i >= 0; i--) {
            reverseStringInput = reverseStringInput + stringInput.charAt(i);
        }

        if (orgStr.equals(reverseStringInput)) {
            System.out.println(orgStr + " is a Palindrome word");
        } else {
            System.out.println(orgStr + " is Not a Palindrome word");
        }
    }
}
