package Labs;

import java.util.Arrays;

public class ReverseArraylist {
    /*
     Reverse an Arraylist.
     */

    static int[] list;

    public static void main(String[] args) {

        int arrayInt[] = {17, 3, 457, 5, 90, 6, 7, 9, 10};

        System.out.println("- Printing original array -");
        System.out.println(Arrays.toString(arrayInt));

        list = new int[arrayInt.length];
        int count = arrayInt.length;
        int k = count - 1;

        //Creating a reverse Array
        for (int i = 0; i < count; i++) {
            list[k] = arrayInt[i];
            k = k - 1;
        }

        System.out.println("- This is a Reverse Arraylist -");
        System.out.println(Arrays.toString(list));
    }
}