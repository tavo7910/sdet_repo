package Labs;

public class StringToCharacter {

    public static void main(String[] args) {
        /*
          Write a program to convert String to a character array and character array to String
         */
        char[] charToString = new char[]{'G', 'U', 'S', 'T', 'A', 'V', 'O'};
        String nameChar = String.valueOf(charToString);
        System.out.println("- Convert character to a String -");
        System.out.println(nameChar);

        String nameString = "GUSTAVO";
        char[] charFromString = nameString.toCharArray();
        System.out.println("- Convert String to a character -");
        for (char c : charFromString) {
            System.out.println(c);
        }
    }
}
