package Labs;

public class ThrowInsideCatch {
	/*
	 Write a program with nested try blocks
	 */

    public ThrowInsideCatch() {
        try {
            thirdBlock();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void thirdBlock() throws Exception {
        try {
            secondBlock();
        } catch (Exception e) {
            throw new Exception("My name is exception Third Block", e);
        }
    }

    public void secondBlock() throws Exception {
        try {
            firstBlock();
        } catch (Exception e) {
            throw new Exception("My name is exception Second BLock", e);
        }
    }

    public void firstBlock() throws Exception {
        throw new Exception("My name is exception First Block");
    }

    public static void main(String... args) {
        new ThrowInsideCatch();
    }
}
