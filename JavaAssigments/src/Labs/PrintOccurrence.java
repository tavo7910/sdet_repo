package Labs;

import java.util.LinkedHashMap;
import java.util.Map;

public class PrintOccurrence {

    public static void main(String[] args) {
        freqCount("DevLabs Alliance Training by Gustavo");
    }

    public static void freqCount(String stringHere) {

        stringHere = stringHere.replaceAll(" ", "");

        Map<Character, Integer> frequenseInt = new LinkedHashMap<Character, Integer>();

        for (char ch : stringHere.toCharArray()) {
            if (frequenseInt.containsKey(ch)) {
                frequenseInt.put(ch, (Integer) frequenseInt.get(ch) + 1);
            } else {
                frequenseInt.put(ch, 1);
            }
        }
        System.out.println(frequenseInt);
    }

}
