package Labs;

import java.util.Scanner;

public class PrimeNumber {

    public static void main(String[] args) {
		/*
		 Is 13 a prime number?
		 */
        Scanner scan = new Scanner(System.in);
        double number = 0, val = 0;
        boolean prime = false;
        System.out.print("Enter a number: -->");
        number = scan.nextInt();

        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                val++;
            }
        }

        if (val == 2) {
            System.out.println("Is " + number + " a prime number? " + true);
        } else {
            System.out.println("Is " + number + " a prime number? " + false);
        }
    }
}
