package Labs;

import java.util.Scanner;

public class SumDigits {

    public static void main(String[] args) {
        /*
        Find the sum of digits
         */

        Scanner scan = new Scanner(System.in);
        int num = 0, sum = 0;

        System.out.println("Type 5 digits (write number and press enter): " );
        for (int i = 0; i < 5; i++) {
            num = scan.nextInt();
            sum = sum + num;
        }
        System.out.println("The result is: " + sum);
    }
}
