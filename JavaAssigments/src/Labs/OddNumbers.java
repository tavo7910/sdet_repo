package Labs;

public class OddNumbers {

    public static void main(String[] args) {
		/*
		Find all the odd numbers from 79 to 187
		 */

        for (int number = 79; number <= 187; number += 2) {
            System.out.println(number);
        }
    }
}
