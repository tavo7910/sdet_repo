package Labs;

import java.util.HashMap;

public class HashSetObjectArray {

    public static void main(String[] args) {
        /*
         Copy all elements of a HashSet to an Object array.
         */

        HashMap<String, String> stringHashMap = new HashMap<String, String>();
        stringHashMap.put("guest2", "223");
        stringHashMap.put("admin", "1234");
        stringHashMap.put("guest1", "332");

        System.out.println("Initial map: " + stringHashMap);
    }
}
